if(typeof browser !== "undefined") {
	browser.proxy.onRequest.addListener(
		() => {
			return {
				type: "http",
				host: "127.0.0.1",
				port: 2370
			};
		},
		{
			urls: ["*://*.zeronet/*"]
		},
		[]
	);
} else {
	chrome.proxy.settings.set(
		{
			value: {
				mode: "pac_script",
				pacScript: {
					data: `
						function FindProxyForURL(url, host) {
							if(!host.endsWith(".zeronet")) {
								return "DIRECT";
							}
							return "PROXY 127.0.0.1:2370";
						}
					`
				}
			},
			scope: "regular"
		},
		() => {}
	);
}


(typeof browser !== "undefined" ? browser : chrome).webRequest.onBeforeRequest.addListener(
	details => {
		if(details.url === "http://127.0.0.1:2370/") {
			return {
				redirectUrl: "http://home.zeronet/"
			};
		} else {
			return {
				type: "direct"
			};
		}
	},
	{
		urls: ["*://127.0.0.1/*"]
	},
	["blocking"]
);


if(typeof browser !== "undefined") {
	(async () => {
		for(const tab of await browser.tabs.query({url: "http://127.0.0.1/"})) {
			if(tab.url === "http://127.0.0.1:2370/") {
				browser.tabs.reload(tab.id);
			}
		}
	})();
} else {
	chrome.tabs.query({url: "http://127.0.0.1/"}, tabs => {
		for(const tab of tabs) {
			if(tab.url === "http://127.0.0.1:2370/") {
				chrome.tabs.reload(tab.id);
			}
		}
	});
}